```mermaid
---
title: Diagramme d'état du chronomètre
config:
  theme: base
  themeVariables:
    primaryColor: "#00ff00"
---
stateDiagram-v2
    [*] --> STOP
    STOP --> START: [BtnA Released]/startTime = milis()
    START --> START: /valChrono = millis() - startTime
    START --> PAUSE: [BtnA Released]/startTime = millis() - startTime
    PAUSE --> STOP: [BtnA Pressed For 1s]/valChrono = 0
    PAUSE --> START: [BtnA Released]/startTime = millis() - startTime
    
    STOP --> STOP: [BtnB Short Press]/Change Text Color
    START --> START: [BtnB Short Press]/Change Text Color
    PAUSE --> PAUSE: [BtnB Short Press]/Change Text Color

    STOP --> STOP: [BtnB Long Press]/Toggle Background Color
    START --> START: [BtnB Long Press]/Toggle Background Color
    PAUSE --> PAUSE: [BtnB Long Press]/Toggle Background Color

    STOP --> STOP: [BtnC Released]/Toggle Tenths Display

```